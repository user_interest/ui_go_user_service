package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"user_interest/ui_go_user_service/config"
	"user_interest/ui_go_user_service/genproto/user_service"
	"user_interest/ui_go_user_service/grpc/client"
	"user_interest/ui_go_user_service/grpc/service"
	"user_interest/ui_go_user_service/pkg/logger"
	"user_interest/ui_go_user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterUserHobbyServiceServer(grpcServer, service.NewUserHobbyService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
